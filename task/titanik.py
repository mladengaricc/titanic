import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()

    selected_titles = ["Mr.", "Mrs.", "Miss."]
    filtered_data = df[df['Name'].str.contains('|'.join(selected_titles))]
    
    median_ages = filtered_data.groupby('Name').median(filtered_data['Age']).round().to_dict()
    
    missing_counts = filtered_data.groupby('Name')['Age'].apply(lambda x: x.isnull().sum()).to_dict()
    
    result = [(title, missing_counts.get(title, 0), int(median_ages.get(title, 0))) for title in selected_titles]
    
    return result


